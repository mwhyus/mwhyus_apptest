import { ScrollView, StyleSheet, Text, TextInput, TouchableOpacity, View, RefreshControl, Alert } from 'react-native'
import React, { useEffect, useState, useCallback } from 'react'
import * as style from '../utils/colors'
import * as eva from '@eva-design/eva';
import { ApplicationProvider, IconRegistry, Icon } from '@ui-kitten/components';
import { EvaIconsPack } from '@ui-kitten/eva-icons';
import Card from '../components/molecules/Card';
import Axios from 'axios';
import { useDispatch } from 'react-redux';

const wait = (timeout) => {
    return new Promise(resolve => setTimeout(resolve, timeout));
  }

const Home = ({navigation}) => {
    const [contacts, setContacts] = useState([]) 
    const [total, setTotal] = useState(0)
    const [refreshing, setRefreshing] = useState(false);
    const [search, setSearch] = useState()
    const dispatch = useDispatch()

    const onRefresh = useCallback(() => {
      setRefreshing(true);
      getData()
      wait(2000).then(() => setRefreshing(false));
    }, []);

    const getData = () => {
        dispatch({type: 'SET_LOADING', value: true})
        Axios.get('https://simple-contact-crud.herokuapp.com/contact')
        .then(res => {
            console.log('ini respon data', res.data.data);
            dispatch({type: 'SET_LOADING', value: false})
            setContacts(res.data.data)
            setTotal(res.data.data.length)
        })
        .catch(err => {
            console.log('ini err', err);
            dispatch({type: 'SET_LOADING', value: false})
            Alert.alert('WARNING', err)
        })
    }

    const handleDataSearch = () => {
        Axios.get('https://simple-contact-crud.herokuapp.com/contact')
        .then(res => {
            setContacts(res.data.data)
            setTotal(res.data.data.length)
        })
        .catch(err => {
            Alert.alert('WARNING', err)
        })
    }

    const searchFilter = (text) => {
        if(text){
            const newData = contacts.filter(item => {
                let name = item.firstName + ' ' + item.lastName;
                const itemData = name ? name.toUpperCase() : ''.toUpperCase();
                const textData = text.toUpperCase();
                return itemData.indexOf(textData) > -1;
            })
            setContacts(newData)
            setSearch(text)
        }else {
            setContacts(contacts)
            setSearch(text)
            handleDataSearch()
        }
    }

    useEffect(() => {
        getData()
    }, [])

  return (
    <>
        <ScrollView 
            style={styles.contactContainer} 
            refreshControl={
                <RefreshControl
                refreshing={refreshing}
                onRefresh={onRefresh}
                />
            }
            >
        <View style={styles.headingContainer}>
            <Text style={styles.heading}>iContact</Text>
            <TouchableOpacity activeOpacity={.8} style={styles.button} onPress={() => navigation.navigate('AddContact')} >
                <IconRegistry icons={EvaIconsPack} />
                    <ApplicationProvider {...eva} theme={eva.light}>
                        <Icon name='plus-outline' fill='white' style={{width: 25, height: 50}} />
                    </ApplicationProvider>
            </TouchableOpacity>
        </View>

        <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Text style={{fontWeight: '700', fontSize: 18, color: style.color}}>{`Total contact: ${total}`}</Text>
        </View>

        <View style={styles.divider}>

        </View>

        <View style={styles.searchContainer}>
            <TextInput 
                placeholder='Search by name...' 
                style={[styles.input, {borderWidth: 3, marginRight: 20}]} 
                value={search}
                onChangeText={(val) => searchFilter(val)}
            />
        </View>

            {contacts.map((contact, index) => {
                if (contact.photo === 'N/A'){
                    contact.photo = 'https://cdn-icons.flaticon.com/png/512/3024/premium/3024605.png?token=exp=1658577558~hmac=2bb1d10c65e2c16161e2da284ec21da0'
                }
                return (
                        <TouchableOpacity 
                            activeOpacity={.8}
                            onPress={()=> navigation.navigate('EditContact', {id: contact.id})} 
                            key={contact.id} >
                            <Card 
                                name={contact.firstName + ' ' + contact.lastName} 
                                age={contact.age} 
                                source={{uri: contact.photo}} 
                            />
                        </TouchableOpacity>
                )
            })}
            <View style={{marginBottom: 20}} />
        </ScrollView>
    </>
  )
}

export default Home

const styles = StyleSheet.create({
    contactContainer: {
        padding: 20,
        opacity: 0.9,
    },
    heading: {
        fontSize: 30,
        fontWeight: '700',
        color: style.color,
    },
    divider: {
        width: '100%',
        height: 2,
        backgroundColor: style.color,
        marginTop: 5,
        marginBottom: 5,
    },
    headingContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    button: {
        backgroundColor: style.color,
        width: 50,
        borderRadius: 100,
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 10,
        marginRight: 10,
        height: 50,
    },
    text: {
        fontWeight: '700',
        fontSize: 17,
        alignSelf: 'center',
    },
    input: {
        height: 40,
        paddingHorizontal: 20,
        width: '100%',
        fontSize: 19,
        color: 'black',
        fontweight: '600',
        opacity: 0.8,
        shadowColor: style.color,
        shadowOpacity: 0.5,
        shadowOffset: {width: 0, height: 4},
        shadowRadius: 8,
        elevation: 5,
        backgroundColor: 'white',
        borderColor: style.color,
        borderWidth: 2,
        borderRadius: 5,
        textAlignVertical: 'center',
    },
    searchContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        marginVertical: 8,
    },
})