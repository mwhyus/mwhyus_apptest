import { KeyboardAvoidingView, ScrollView, StyleSheet, Text, View, Keyboard, Image, TouchableOpacity, Alert } from 'react-native'
import React, {useState, useEffect} from 'react'
import * as style from '../utils/colors'
import { TouchableWithoutFeedback } from '@ui-kitten/components/devsupport'
import Input from '../components/molecules/Input'
import Axios from 'axios'
import {launchImageLibrary} from 'react-native-image-picker'
import { useRoute } from '@react-navigation/native'
import { api } from '../utils/api'
import { ILDelete2 } from '../assets'
import DeleteButton from '../components/atoms/DeleteButton'
import SaveButton from '../components/atoms/SaveButton'
import BackButton from '../components/atoms/BackButton'
import { useDispatch } from 'react-redux'

const EditContact = ({navigation}) => {
    const [firstName, setFirstName] = useState('')
    const [lastName, setLastName] = useState('')
    const [age, setAge] = useState('')
    const [photo, setPhoto] = useState('')
    const [photoForDB, setPhotoForDB] = useState('')
    const dispatch = useDispatch()

    const route = useRoute()

    const getData = () => {
        dispatch({type: 'SET_LOADING', value: true})
        Axios.get(`${api}/contact/${route.params.id}`)
        .then(res => {
            dispatch({type: 'SET_LOADING', value: false})
            console.log('ini respon cuyyy', res.data.data);
            setFirstName(res.data.data.firstName)
            setLastName(res.data.data.lastName)
            let age = res.data.data.age
            let newAge = age.toString()
            setAge(newAge)
        })
        .catch(err => {
            console.log('ini err', err);
            dispatch({type: 'SET_LOADING', value: false})
        })
    }

    const addPhoto = () => {
        launchImageLibrary(
            {quality: 0.5,maxWidth: 200, maxHeight: 200, includeBase64: true },
           response => {
            if (response.didCancel || response.error) {
                console.log('User cancelled image picker');
                Alert.alert('Please choose photo')
            } else {
                console.log('Response: ', response);
                const source = {uri: response.assets[0].uri};
                console.log('source: ', source);
                const srcPhotoForDB = response.assets[0].uri
                console.log('srcPhotoForDB: ', srcPhotoForDB);
                setPhoto(source)
                setPhotoForDB(srcPhotoForDB)
            }
        })
    }

    const save = () => {
        const payload = {
            firstName,
            lastName,
            age,
            photo: photoForDB,
        }
        console.log('data : ', payload);
        dispatch({type: 'SET_LOADING', value: true})
        Axios.put(`${api}/contact/${route.params.id}`, payload)
        .then(res => {
            console.log('ini respon', res);
            dispatch({type: 'SET_LOADING', value: false})
            Alert.alert(res.data.message.toUpperCase(), 'Pull To Refresh')
            setAge()
            setFirstName('')
            setLastName('')
            navigation.navigate('Home')
        }).catch(err => {
            dispatch({type: 'SET_LOADING', value: false})
            console.log('ini err', err);
            Alert.alert('WARNING', 'Data is invalid')
        })
    }

    const deleteContact = () => {
        Alert.alert(
            'WARNING', `Are you sure want to delete ${firstName + ' ' + lastName}?`,
            [
                {text: 'Cancel', style: 'cancel'},
                {text: 'OK', 
                onPress: () => {
                    Axios.delete(`${api}/contact/${route.params.id}`)
                    .then(res => {
                        console.log('ini respon', res);
                        Alert.alert(res.data.message.toUpperCase(), 'Pull To Refresh')
                        navigation.navigate('Home')
                    }).catch(err => {
                        console.log('ini err', err);
                        Alert.alert('WARNING', err.response.data.message)
                    }
                )
                }}
            ])
    }

    useEffect(() => {
        console.log('ini id contact', route.params.id);
        getData()
        console.log('ini umur', age);
    }, [])

  return (
    <>
        <ScrollView style={{flex: 1, padding: 20}}>
            <KeyboardAvoidingView
            behavior={Platform.OS ==='ios' ? 'padding' : 'height'}
            >
                <BackButton onPress={() => navigation.goBack()} />
                <View>
                    <TouchableOpacity style={styles.photo} activeOpacity={0.8} onPress={addPhoto}>
                        <View style={styles.borderPhoto}>
                            {photo ? (
                                <Image source={photo} style={styles.photoContainer} /> 
                                ) : ( <View style={styles.photoContainer}>
                                    <Text style={styles.photoText}>Add Photo</Text>
                                </View>
                            )}
                        </View>
                    </TouchableOpacity>
                </View>

                <TouchableWithoutFeedback onPress={Keyboard.dismiss} style={{}}>
                    <Input value={firstName} onChangeText={(val) => setFirstName(val)} title='First Name' />
                    <Input value={lastName} onChangeText={(val) => setLastName(val)} title='Last Name' />
                    <Input value={age} onChangeText={(val) => setAge(val)} title='Age' />
                </TouchableWithoutFeedback>
            </KeyboardAvoidingView>
            <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                <DeleteButton source={ILDelete2} onPress={deleteContact} /> 
                <SaveButton onPress={save} />
            </View>
        </ScrollView>
    </>
  )
}

export default EditContact

const styles = StyleSheet.create({
    photo: {
        alignItems: 'center',
        marginTop: 26,
        marginBottom: 16
    },
    borderPhoto: {
        borderWidth: 1,
        borderColor: style.color,
        width: 110,
        height: 110,
        borderRadius: 110,
        borderStyle: 'dashed',
        justifyContent: 'center',
        alignItems: 'center',
    },
    photoContainer: {
        width: 90,
        height: 90,
        borderRadius: 90,
        backgroundColor: '#F0F0F0',
        padding: 24,
    },
    photoText: {
        fontSize: 14,
        fontFamily: 'Poppins-Light',
        color: '#8D92A3',
        textAlign: 'center'
    },

})