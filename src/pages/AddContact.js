import { KeyboardAvoidingView, ScrollView, StyleSheet, Text, View, Keyboard, Image, TouchableOpacity, Alert } from 'react-native'
import React, {useState} from 'react'
import * as style from '../utils/colors'
import { TouchableWithoutFeedback } from '@ui-kitten/components/devsupport'
import Input from '../components/molecules/Input'
import Axios from 'axios'
import {launchImageLibrary} from 'react-native-image-picker'
import useForm from '../utils/useForm'
import SaveButton from '../components/atoms/SaveButton'
import BackButton from '../components/atoms/BackButton'
import { useDispatch } from 'react-redux'
import { api } from '../utils/api'

const AddContact = ({navigation}) => {
    const [photo, setPhoto] = useState('')
    const [photoForDB, setPhotoForDB] = useState('')
    const dispatch = useDispatch()
    const [form, setForm] = useForm({
        firstName: '',
        lastName: '',
        age: '',
    })


    const addPhoto = () => {
        launchImageLibrary(
            {quality: 0.5,maxWidth: 200, maxHeight: 200, includeBase64: true },
           response => {
            if (response.didCancel || response.error) {
                console.log('User cancelled image picker');
                Alert.alert('Please choose photo')
            } else {
                console.log('Response: ', response);
                const source = {uri: response.assets[0].uri};
                console.log('source: ', source);
                const srcPhotoForDB = response.assets[0].uri
                console.log('srcPhotoForDB: ', srcPhotoForDB);
                setPhoto(source)
                setPhotoForDB(srcPhotoForDB)
            }
        })
    }


    const save = () => {
        const payload = {
            firstName: form.firstName,
            lastName: form.lastName,
            age: form.age,
            photo: photoForDB,
        }
        console.log('data : ', payload);
        dispatch({type: 'SET_LOADING', value: true})
        Axios.post(`${api}/contact`, payload)
        .then(res => {
            console.log('ini respon', res);
            dispatch({type: 'SET_LOADING', value: false})
            Alert.alert(res.data.message.toUpperCase(), 'Pull To Refresh')
            navigation.navigate('Home')
        }).catch(err => {
            console.log('ini err', err);
            dispatch({type: 'SET_LOADING', value: false})
            Alert.alert('WARNING', 'Data is invalid')
        })
    }

  return (
    <>
        <ScrollView style={{flex: 1, padding: 20}}>
            <KeyboardAvoidingView
            behavior={Platform.OS ==='ios' ? 'padding' : 'height'}
            >
                <BackButton onPress={() => navigation.goBack()} />
                <View>
                    <TouchableOpacity activeOpacity={.8} style={styles.photo} onPress={addPhoto}>
                        <View style={styles.borderPhoto}>
                            {photo ? (
                                <Image source={photo} style={styles.photoContainer} /> 
                                ) : ( <View style={styles.photoContainer}>
                                    <Text style={styles.photoText}>Add Photo</Text>
                                </View>
                            )}
                        </View>
                    </TouchableOpacity>
                </View>

                <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                    <Input value={form.firstName} onChangeText={(val) => setForm('firstName', val)} title='First Name' />
                    <Input value={form.lastName} onChangeText={(val) => setForm('lastName',val)} title='Last Name' />
                    <Input value={form.age} onChangeText={(val) => setForm('age', val)} title='Age' />
                </TouchableWithoutFeedback>
            </KeyboardAvoidingView>
                <View style={{marginLeft: '22%'}}>
                    <SaveButton onPress={save} />
                </View>
        </ScrollView>
    </>
  )
}

export default AddContact

const styles = StyleSheet.create({
    button: {
        backgroundColor: style.color,
        width: '40%',
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center',
        height: 40,
        alignSelf: 'flex-end',
        marginTop: 20,
        marginRight: 20
    },
    photo: {
        alignItems: 'center',
        marginTop: 26,
        marginBottom: 16
    },
    borderPhoto: {
        borderWidth: 1,
        borderColor: style.color,
        width: 110,
        height: 110,
        borderRadius: 110,
        borderStyle: 'dashed',
        justifyContent: 'center',
        alignItems: 'center',
    },
    photoContainer: {
        width: 90,
        height: 90,
        borderRadius: 90,
        backgroundColor: '#F0F0F0',
        justifyContent: 'center',
        alignItems: 'center',
    },
    addPhoto: {
        fontSize: 14,
        fontFamily: 'Poppins-Light',
        color: '#8D92A3',
        textAlign: 'center'
    },
    photoText: {
        fontSize: 14,
        fontFamily: 'Poppins-Light',
        color: '#8D92A3',
        textAlign: 'center'
    },

})