import {createStore} from 'redux';

const initTialState = {
    loading: false,
}

const reducer = (state = initTialState, action) => {
    if(action.type === 'SET_LOADING') {
        return{
            ...state,
            loading: action.value
        }
    }
    return state;
};

const store = createStore(reducer)

export default store;