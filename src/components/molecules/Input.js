import { StyleSheet, Text, View, TextInput } from 'react-native'
import React from 'react'
import * as style from '../../utils/colors'

const Input = ({title, onChangeText, value}) => {
  return (
    <View style={{padding: 20, justifyContent: 'space-around'}}>
        <Text style={styles.text}>{title}</Text>
        <TextInput value={value} onChangeText={onChangeText} style={[styles.input]} />
    </View>
  )
}

export default Input

const styles = StyleSheet.create({
    input: {
        padding: 10,
        width: '100%',
        fontSize: 12,
        color: 'black',
        fontWeight: '600',
        opacity: 0.5,
        shadowColor: style.color,
        shadowOpacity: 0.4,
        shadowOffset: {width: 0, height: 4},
        shadowRadius: 8,
        elevation: 5,
        backgroundColor: 'white',
        borderColor: style.color,
        borderWidth: 2,
        borderRadius: 5,
        height: 50,
    },
    text: {
        alignSelf: 'center',
        fontSize: 20,
        fontWeight: '700',
        color: style.color,
        marginRight: 14
    }
})