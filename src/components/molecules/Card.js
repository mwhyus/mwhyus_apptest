import { StyleSheet, Text, TouchableOpacity, View, Image } from 'react-native'
import React from 'react'
import * as style from '../../utils/colors'


const Card = ({name, age, source}) => {
  return (
    <View style={styles.item}>
        <View>
            <View style={styles.Contact}>
                <Text style={styles.index}>Name: </Text>
                <Text style={styles.text}>{name}</Text>
            </View>
            <View style={styles.Contact}>
                <Text style={styles.index}>Age: </Text>
                <Text style={styles.text}>{age}</Text>
            </View>
        </View>
        <View>
            <View style={styles.photo}>
                <View style={styles.borderPhoto}>
                    <View style={styles.photoContainer}>
                        <Image source={source} style={styles.photoContainer} />
                    </View>
                </View>
            </View>
        </View>
    </View>
  )
}

export default Card

const styles = StyleSheet.create({
    item: {
        marginBottom: 20,
        padding: 10,
        color: 'black',
        opacity: 0.9,
        marginTop: 10,
        shadowColor: style.color,
        shadowOpacity: 0.5,
        shadowOffset: {width: 0, height: 4},
        shadowRadius: 8,
        elevation: 5,
        backgroundColor: 'white',
        borderColor: style.color,
        borderWidth: 2,
        borderRadius: 5,
        borderLeftWidth: 15,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    Contact: {
        flexDirection: 'row',
        width: '75%',
    },
    index: {
        fontSize: 20,
        fontWeight: '800',
    },
    text: {
        fontWeight: '700',
        fontSize: 17,
        alignSelf: 'center',
    },
    photo: {
        alignItems: 'center',
        marginVertical: 5
    },
    borderPhoto: {
        borderWidth: 1,
        borderColor: style.color,
        width: 55,
        height: 55,
        borderRadius: 110,
        borderStyle: 'dashed',
        justifyContent: 'center',
        alignItems: 'center',
    },
    photoContainer: {
        width: 45,
        height: 45,
        borderRadius: 90,
        backgroundColor: '#F0F0F0',
        padding: 14,
    },
    addPhoto: {
        fontSize: 12,
        fontFamily: 'Poppins-Light',
        color: '#8D92A3',
        textAlign: 'center'
    },
    photoContainer: {
        width: 45,
        height: 45,
        borderRadius: 90,
        backgroundColor: '#F0F0F0',
        // padding: 24,
        justifyContent: 'center',
        alignItems: 'center',
    },

})