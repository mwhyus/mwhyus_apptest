import { ActivityIndicator, StyleSheet, Text, View } from 'react-native'
import React from 'react'
import * as style from '../../utils/colors'

const Loading = () => {
  return (
    <View style={styles.wrapper}>
        <ActivityIndicator size="large" color={style.color} />
      <Text style={styles.text}>Loading...</Text>
    </View>
  )
}

export default Loading

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        position: 'absolute',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        height: '100%',
        backgroundColor: 'rgba(0,0,0,0.5)',
    },
    text: {
        fontSize: 18,
        color: 'white',
        marginTop: 6,
        // alignSelf: 'center',
    }

})