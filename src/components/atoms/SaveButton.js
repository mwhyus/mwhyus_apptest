import { StyleSheet, TouchableOpacity, Text } from 'react-native'
import React from 'react'
import * as style from '../../utils/colors'

const SaveButton = ({onPress}) => {
  return (
    <TouchableOpacity style={styles.buttonSave} activeOpacity={.7} onPress={onPress}>
        <Text style={styles.buttonText}>Save</Text>
    </TouchableOpacity>
  )
}

export default SaveButton

const styles = StyleSheet.create({
    buttonSave: {
        backgroundColor: style.color,
        width: '70%',
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center',
        height: 40,
        // alignSelf: 'flex-end',
        marginTop: 20,
        marginBottom: 4,
        marginRight: 20
    },
    buttonText: {
        color: 'white',
        fontSize: 20,
        fontWeight: '700',
    },
})