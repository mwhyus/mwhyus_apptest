import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React from 'react'
import { ILBack } from '../../assets'

const BackButton = ({onPress}) => {
  return (
    <TouchableOpacity onPress={onPress}>
        <ILBack  />
    </TouchableOpacity>
  )
}

export default BackButton

const styles = StyleSheet.create({})