import { StyleSheet, TouchableOpacity, Image } from 'react-native'
import React from 'react'
import * as style from '../../utils/colors'

const DeleteButton = ({source, onPress}) => {
  return (
    <TouchableOpacity activeOpacity={0.8} style={styles.buttonDelete} onPress={onPress}>
        <Image source={source} style={{width: 30, height: 30}} />
    </TouchableOpacity>
  )
}

export default DeleteButton

const styles = StyleSheet.create({
    buttonDelete: {
        backgroundColor: style.colorDanger,
        width: 90/2,
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center',
        height: 90/2,
        alignSelf: 'flex-end',
        marginLeft: 20,
    },
})