import React, {useState} from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Home from './pages/Home'
import AddContact from './pages/AddContact';
import EditContact from './pages/EditContact';
import Loading from './components/atoms/Loading';
import { Provider, useSelector } from 'react-redux';
import store from './utils/redux/store';
import { LogBox } from 'react-native';

const Stack = createNativeStackNavigator() 

const MainApp = () => {
const stateGlobal = useSelector(state => state)
console.log('ini state global', stateGlobal);
LogBox.ignoreAllLogs()
  return (
    <>
      <NavigationContainer >
        <Stack.Navigator 
          screenOptions={{
            headerShown: false
          }}
        >
          <Stack.Screen name="Home" component={Home} headerMode={false}/>
          <Stack.Screen name="AddContact" component={AddContact} headerMode={false} />
          <Stack.Screen name="EditContact" component={EditContact} headerMode={false} />
        </Stack.Navigator>
      </NavigationContainer>
      {stateGlobal.loading && <Loading />}
    </>
  )
}

const App = () => {
  return (
    <Provider store={store}>
      <MainApp />
    </Provider>
  )
}

export default App
